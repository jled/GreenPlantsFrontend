import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http/';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { AgmCoreModule } from '@agm/core';
import { JwtModule } from '@auth0/angular-jwt';
import { Ng2CloudinaryModule } from 'ng2-cloudinary';
import { FileUploadModule } from 'ng2-file-upload';
import { ChartsModule } from 'ng2-charts';

import { AppComponent } from './app.component';
import { ConnectionComponent } from './components/connection/connection.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ConnectComponent } from './components/connection/connect/connect.component';
import { LoginComponent } from './components/connection/login/login.component';
import { RegisterComponent } from './components/connection/register/register.component';
import { PlantsComponent } from './components/dashboard/plants/plants.component';
import { ConfigurationComponent } from './components/dashboard/configuration/configuration.component';
import { AddPlantComponent } from './components/dashboard/add-plant/add-plant.component';
import { EditPlantComponent } from './components/dashboard/edit-plant/edit-plant.component';
import { ViewPlantComponent } from './components/dashboard/view-plant/view-plant.component';

import { PlantService } from './services/plant.service';
import { ConnectionService } from './services/connection.service';
import { Authguard } from './services/authguard.service';





const appRoutes: Routes = [
  { path: '', component: ConnectionComponent, children: [
      {path: '', component: ConnectComponent},
      {path: 'login', component: LoginComponent}
  ]},
  { path: 'register', component: RegisterComponent},
  { path: 'dashboard', component: DashboardComponent, canActivate: [Authguard], canActivateChild: [Authguard], children: [
    {path: '', component: PlantsComponent},
    {path: 'configuration', component: ConfigurationComponent},
    {path: 'add', component: AddPlantComponent},
    {path: 'edit/:id', component: EditPlantComponent},
    {path: 'view/:id', component: ViewPlantComponent}
  ]}
];

@NgModule({
  declarations: [
    AppComponent,
    ConnectionComponent,
    DashboardComponent,
    ConnectComponent,
    LoginComponent,
    RegisterComponent,
    PlantsComponent,
    ConfigurationComponent,
    AddPlantComponent,
    EditPlantComponent,
    ViewPlantComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    AgmCoreModule.forRoot({
      apiKey:  'my_API_key'
    }),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter
      }
    }),
    Ng2CloudinaryModule,
    FileUploadModule,
    ChartsModule
  ],
  providers: [ConnectionService, PlantService, Authguard],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function tokenGetter() {
  return localStorage.getItem('token');
}
