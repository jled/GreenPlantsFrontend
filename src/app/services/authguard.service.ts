import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild } from '@angular/router/src/interfaces';
import { PlantService } from './plant.service';
import { Router } from '@angular/router';

@Injectable()
export class Authguard implements CanActivate, CanActivateChild {

  constructor(private plantService: PlantService, private router: Router) { }

  async canActivate() {
    const ans = await this.plantService.isAuthenticated();
    if (ans) {
      return true;
    } else {
      localStorage.removeItem('ipAddress');
      localStorage.removeItem('token');
      this.router.navigate(['']);
      return false;
    }
  }

  async canActivateChild() {
    const ans = await this.plantService.isAuthenticated();
    if (ans) {
      return true;
    } else {
      localStorage.removeItem('ipAddress');
      localStorage.removeItem('token');
      this.router.navigate(['']);
      return false;
    }
  }

}
