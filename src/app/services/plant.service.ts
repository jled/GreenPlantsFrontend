import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
// tslint:disable-next-line:import-blacklist
import 'rxjs/Rx';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { CloudinaryUploader } from 'ng2-cloudinary/dist/esm/src/cloudinary-uploader.service';
import { CloudinaryOptions } from 'ng2-cloudinary/dist/esm/src/cloudinary-options.class';

@Injectable()
export class PlantService {
  private SECRET = 'mysecret';

  private ipAddress: String;
  private token: String;

  public imageUrl: String;
  public imageLoaded: Boolean = true;
  public errorImage: Boolean = false;

  uploader: CloudinaryUploader = new CloudinaryUploader(
    new CloudinaryOptions({
      cloudName: 'greenplants',
      uploadPreset: 'unsigned'
    })
  );

  constructor(public http: Http, public jwtHelper: JwtHelperService) {
    this.token = localStorage.getItem('token');
    this.ipAddress = localStorage.getItem('ipAddress');

    this.uploader.onBeforeUploadItem = (fileItem) => {
      this.imageUrl = 'assets/img/loadingImage.gif';
      this.imageLoaded = false;
    };

    this.uploader.onSuccessItem = (item: any, response: string, status: number, headers: any): any => {
      const res: any = JSON.parse(response);
      res.url = res.url.replace('v' + res.version, 'w_400,h_400,c_fill');
      this.imageUrl = res.url;
      this.imageLoaded = true;
    };

    this.uploader.onErrorItem = (item: any, response: string, status: number, headers: any): any => {
      this.imageLoaded = true;
      this.imageUrl = 'http://res.cloudinary.com/greenplants/image/upload/w_400,h_400,c_fill/sample.jpg';
    };
  }

  getPlants() {
    this.token = localStorage.getItem('token');
    this.ipAddress = localStorage.getItem('ipAddress');
    return this.http.get('http://' + this.ipAddress + ':8000/api/plants?token=' + this.token)
      .map((response: Response) => {
        return response.json();
      }).catch((err) => {
        return Observable.throw(err);
      });
  }

  getPlant(id) {
    this.token = localStorage.getItem('token');
    this.ipAddress = localStorage.getItem('ipAddress');
    return this.http.get('http://' + this.ipAddress + ':8000/api/plant/' + id + '?token=' + this.token)
      .map((response: Response) => {
        return response.json();
      }).catch((err) => {
        return Observable.throw(err);
      });
  }

  addPlants(name, description, servo, apertureTime, caudal, maxLastOpenings,
    weather, exception1, exception1Condition, exception1Value,
    exception2, exception2Condition, exception2Value,
    exception3, exception3Condition, exception3Value,
    exception4, exception4Condition, exception4Value,
    timeLapse) {
    let objectPost;
    objectPost = {
      name: name,
      description: description,
      servo: servo,
      tiempo_abertura: apertureTime,
      photo: this.imageUrl,
      caudal: caudal,
      conditions: [
        {
          enabled: exception3,
          sensor: 1,
          condition: exception3Condition,
          value: exception3Value
        },
        {
          enabled: exception4,
          sensor: 2,
          condition: exception4Condition,
          value: exception4Value
        }
      ],
      maxLastOpening: maxLastOpenings,
      weatherAfected: weather,
      timeConditions: {
        value: timeLapse,
        exceptions: [{
          enabled: exception1,
          sensor: 1,
          condition: exception1Condition,
          value: exception1Value
        },
        {
          enabled: exception2,
          sensor: 2,
          condition: exception2Condition,
          value: exception2Value
        }
        ]
      }

    };
    console.log(objectPost);
    return this.http.post('http://' + this.ipAddress + ':8000/api/plants?token=' + this.token, objectPost)
      .timeout(10000)
      .map((response: Response) => {
        return response.json();
      }).catch((err) => {
        return Observable.throw(err);
      });
  }

  editPlants(plant) {
    return this.http.patch('http://' + this.ipAddress + ':8000/api/plant/' + plant._id + '?token=' + this.token, plant)
      .timeout(10000)
      .map((response: Response) => {
        return response.json();
      }).catch((err) => {
        return Observable.throw(err);
      });
  }


  getWeather() {
    this.token = localStorage.getItem('token');
    this.ipAddress = localStorage.getItem('ipAddress');
    return this.http.get('http://' + this.ipAddress + ':8000/api/weather?token=' + this.token)
      .map((response: Response) => {
        return response.json();
      });
  }

  getConfiguration() {
    return this.http.get('http://' + this.ipAddress + ':8000/api/configuration?token=' + this.token)
      .timeout(10000)
      .map((response: Response) => {
        return response.json();
      }).catch((err) => {
        return Observable.throw(err);
      });
  }

  changeConfiguration(newConfiguration) {
    return this.http.post('http://' + this.ipAddress + ':8000/api/configuration?token=' + this.token, newConfiguration)
      .timeout(10000)
      .map((response: Response) => {
        return response.json();
      }).catch((err) => {
        return Observable.throw(err);
      });
  }

  changePassword(passwords) {
    return this.http.post('http://' + this.ipAddress + ':8000/api/password?token=' + this.token, passwords)
      .timeout(10000)
      .map((response: Response) => {
        return response.json();
      }).catch((err) => {
        return Observable.throw(err);
      });
  }

  isAuthenticated() {
    return new Promise((resolve) => {
      const token = this.jwtHelper.tokenGetter();
      if (!token) {
        resolve(false);
      }

      const expired = this.jwtHelper.isTokenExpired(token);

      if (expired) {
        resolve(false);
      }

      this.getPlants()
        .subscribe(data => {
          if (data.status > 400) {
            resolve(false);
          }
          console.log(data);
          resolve(true);
        },
        (err) => {
          resolve(false);
        });
    });
  }

  resetConfiguration(): any {
    return this.http.post('http://' + this.ipAddress + ':8000/api/reset?token=' + this.token, {})
      .timeout(10000)
      .map((response: Response) => {
        return response.json();
      }).catch((err) => {
        return Observable.throw(err);
      });
  }

  deletePlant(plant): any {
    return this.http.delete('http://' + this.ipAddress + ':8000/api/plant/' + plant._id + '?token=' + this.token, {})
      .timeout(10000)
      .map((response: Response) => {
        return response.json();
      }).catch((err) => {
        return Observable.throw(err);
      });
  }

  async sleep(ms) {
    await new Promise((resolve) => setTimeout(resolve, ms));
  }
}
