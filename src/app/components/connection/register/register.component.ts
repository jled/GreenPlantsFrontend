import { Component, OnInit } from '@angular/core';
import { ConnectionService } from '../../../services/connection.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  lat: Number = 43.134906;
  lng: Number = -2.076319;
  connecting: Boolean = false;
  private errors: Boolean = false;
  private error: any;
  private ipAddress: String;
  marker: Marker;

  constructor(private connectionService: ConnectionService, private router: Router) { }

  ngOnInit() {
    this.marker = {
      lat: 43.134906,
      lng: -2.076319,
    };
    if (!!navigator.geolocation) {
      // Support
      navigator.geolocation.getCurrentPosition((position) => {
        this.marker.lat = position.coords.latitude;
        this.marker.lng = position.coords.longitude;
      },
        () => {
          console.log('Error picking geolocation position');
        },
        { maximumAge: 600000 });

    } else {
      console.log('Error picking geolocation position');
    }
  }

  mapClicked(event) {
    this.marker.lat = event.coords.lat;
    this.marker.lng = event.coords.lng;
  }

  markerDragEnd(event) {
    this.marker.lat = event.coords.lat;
    this.marker.lng = event.coords.lng;
  }

  onSubmit(event) {
    this.connecting = true;
    this.ipAddress = localStorage.getItem('ipAddress');
    this.connectionService.register(event.name, event.password, this.marker.lat + ',' + this.marker.lng)
    .subscribe(data => {
      if (data.status > 500) {
        this.errors = true;
        this.error = data.error;
        this.connecting = false;
      } else if (data.status === 200) {
        this.router.navigate(['']);
      }
    },
    (err) => {
      if (err.name === 'TimeoutError') {
        this.error = 'Doesn\'t look like a GreenPlants device IP';
      } else {
        this.error = 'Incorrect URI or IP';
      }
      this.connecting = false;
      this.errors = true;
    });

  }
}

interface Marker {
  lat: number;
  lng: number;
}
