import { Component, OnInit } from '@angular/core';

import { ConnectionService } from '../../../services/connection.service';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public connecting: Boolean = false;
  private ipAddress: String;
  private errors: Boolean = false;
  private error: any;
  private password: String;

  constructor(private connectionService: ConnectionService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit(input) {
    this.connecting = true;
    this.errors = false;
    this.ipAddress = localStorage.getItem('ipAddress');
    this.password = input.password;
    this.connectionService.login(this.password)
      .subscribe(data => {
        console.log(data);
        if (data.status === 200) {
          localStorage.setItem('token', data.token);
          this.router.navigate(['dashboard']);
        } else if (data.status === 500) {
          this.errors = true;
          this.connecting = false;
          this.error = 'Incorrect password';
        } else {
          this.errors = true;
          this.connecting = false;
          this.error = 'Error with the GreenPlants server';
        }
      },
      err => {
        if (err.name === 'TimeoutError') {
          this.connecting = false;
          this.errors = true;
          this.error = 'Error with the GreenPlants device';
        }
      }
      );
  }

}
