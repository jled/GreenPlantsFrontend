import { Component, OnInit, ViewChild } from '@angular/core';
import { PlantService } from '../../../services/plant.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-plant',
  templateUrl: './edit-plant.component.html',
  styleUrls: ['./edit-plant.component.css']
})
export class EditPlantComponent implements OnInit {

  success: Boolean = false;
  errors: Boolean = false;
  error: String = '';

  private id: any;
  private plant: any = {};


  constructor(private plantService: PlantService, private routerParams: ActivatedRoute, private router: Router) {
    this.plant.timeConditions = {
      value: 24,
      exceptions: [
        {enabled: false},
        {enabled: false}
      ],
    };
    this.plant.conditions = [{

    },
    {}];
    console.log(this.plant);
  }

  ngOnInit() {
    this.id = this.routerParams.snapshot.paramMap.get('id');
    this.plantService.getPlant(this.id)
    .subscribe(data => {
      this.plant = data.plant[0];
      if (data.status > 400) {
      }
      console.log(this.plant);
      this.plantService.imageUrl = this.plant.photo;
    },
    (err) => {
    });
  }

  onChange(file) {
    this.plantService.uploader.uploadAll();
  }

  onSubmit(form) {
    window.moveBy(0, 0);
    this.success = false;
    this.errors = false;
    this.plant.servo = parseInt(this.plant.servo, 10);
    this.plant.photo = this.plantService.imageUrl;
    delete this.plant.sensor_1;
    delete this.plant.sensor_2;
    if (this.plant.servo > 2) {
      this.errors = true;
      this.error = 'El Servo numero ' + this.plant.servo + ' no está activado';
      return;
    }
    this.plantService.editPlants(this.plant)
      .subscribe(data => {
        console.log(data);
        if (data.status === 200) {
          this.success = true;
          setTimeout(() => this.router.navigate(['dashboard']), 1000);
        } else {
          this.errors = true;
          if (data.error.hasOwnProperty('message')) {
              if (data.error.message.indexOf('unique') !== -1) {
                if (data.error.message.indexOf('servo') !== -1) {
                  this.error = 'El servo debe ser único para cada plantación';
                } else {
                  this.error = 'El nombre debe ser único para cada plantación';
                }
              } else if (data.error.message.indexOf('maximum allowed') !== -1) {
                this.error = 'Limite de caracteres superado';
              }else {
                this.error = 'Rellene todas las casillas';
              }
            } else {
              this.error = data.error;
            }
        }
      },
      err => console.log('ERROR: ' + err));
  }

}

/*interface SensorData {
  date:   Date;
  value:  Number;
}

interface Conditions {
  enabled:    Boolean;
  sensor:     Number;
  condition:  String;
  value:      Number;
}

interface TimeConditions {
  value:      Number;
  exceptions: [Conditions];
}

interface Plant {
  name:             String;
  description:      String;
  servo:            Number;
  sensor_1:         Number;
  sensor_2:         Number;
  sensor_1Data:     [SensorData];
  sensor_2Data:     [SensorData];
  ultimo_riego:     [Date];    // Data
  tiempo_abertura:  Number; // Segundos
  caudal:           Number;
  weatherAfected:   Boolean;
  maxLastOpening:   Number;
  timeConditions:   TimeConditions;
  conditions:       [Conditions];
  enabled:          Boolean;
  photo:            String;
}
*/
