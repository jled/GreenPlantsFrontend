import { Component, OnInit } from '@angular/core';
import { ConnectionService } from '../../../services/connection.service';
import { PlantService } from '../../../services/plant.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.css']
})
export class ConfigurationComponent implements OnInit {

  public passwordErrors: Boolean = false;
  private passwordError: any;
  public passwordSuccess: Boolean = false;
  public success: Boolean = false;
  public errors: Boolean = false;
  private error: any;
  marker: Marker = <Marker>{};
  configuration: Configuration = <Configuration>{};

  constructor(private plantService: PlantService, private router: Router) {

  }

  ngOnInit() {
    this.plantService.getConfiguration()
      .subscribe(data => {
        if (data.status > 500) {
          console.log('ERROR');
        } else {
          const coords = data.data.location.split(',');
          this.marker.lat = parseFloat(coords[0]);
          this.marker.lng = parseFloat(coords[1]);
          this.configuration.name = data.data.name;
          this.configuration.updateTime = data.data.updateTime;
        }

      },
      err => {
        console.log('ERROR:' + err);
      });
  }

  mapClicked(event) {
    this.marker.lat = event.coords.lat;
    this.marker.lng = event.coords.lng;
  }

  markerDragEnd(event) {
    this.marker.lat = event.coords.lat;
    this.marker.lng = event.coords.lng;
  }

  onSubmit(form) {
    form.location = this.marker.lat + ',' + this.marker.lng;
    this.errors = false;
    this.success = false;
    this.plantService.changeConfiguration(form)
      .subscribe(data => {
        console.log(data);
        if (data.status >= 500) {
          this.errors = true;
          this.error = data.error;
        }
        if (data.status === 200) {
          this.success = true;
        }
      },
      err => {
        this.errors = true;
        this.error = err;
      });
  }

  onSubmitPassword(form) {
    this.passwordErrors = false;
    this.passwordSuccess = false;
    this.plantService.changePassword(form)
      .subscribe(data => {
        console.log(data);
        if (data.status >= 500) {
          this.passwordErrors = true;
          this.passwordError = data.error;
        }
        if (data.status === 200) {
          this.passwordSuccess = true;
        }
      },
      err => {
        this.passwordErrors = true;
        this.passwordError = err;
      });
  }

  resetConfiguration() {
    const answer = confirm('¿Esta seguro que desea reiniciar la aplicación?');

    if (answer) {
      this.plantService.resetConfiguration()
      .subscribe(data => {
        console.log(data);
        localStorage.removeItem('ipAddress');
        localStorage.removeItem('token');
        this.router.navigate(['']);
      },
      err => {
        console.log(err);
        this.passwordErrors = true;
        this.passwordError = err;
      });
    }
  }

}



interface Marker {
  lat: number;
  lng: number;
}

interface Configuration {
  name: String;
  updateTime: Number;
}
