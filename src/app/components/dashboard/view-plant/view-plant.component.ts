import { Component, OnInit } from '@angular/core';
import { PlantService } from '../../../services/plant.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view-plant',
  templateUrl: './view-plant.component.html',
  styleUrls: ['./view-plant.component.css']
})
export class ViewPlantComponent implements OnInit {

  id: String;
  plant: any = {};

  public lineChartData1: Array<any> = [
    {data: [], label: 'Humedad'},
  ];

  public lineChartData2: Array<any> = [
    {data: [], label: 'Temperatura'},
  ];

  public lineChartLabels: Array<any> = [];
  public lineChartOptions: any = {
    responsive: true
  };
  public lineChartLegend: Boolean = false;
  public lineChartType: String = 'line';

  public humidityColors: Array<any> = [
    { // first color
      backgroundColor: 'rgba(24,10,254,0.2)',
      borderColor: 'rgba(24,10,255,0.2)',
      pointBackgroundColor: 'rgba(10,10,225,0.2)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(225,10,24,0.2)'
    }
  ];

  constructor(private plantService: PlantService, private routerParams: ActivatedRoute) {
    this.plant.timeConditions = {
      value: 24,
      exceptions: [
        {enabled: false},
        {enabled: false}
      ],
    };
    this.plant.conditions = [{

    },
    {}];
    this.plant.ultimo_riego = [];
  }

  ngOnInit() {
    this.id = this.routerParams.snapshot.paramMap.get('id');
    this.plantService.getPlant(this.id)
    .subscribe(data => {
      this.plant = data.plant[0];
      if (data.status > 400) {
      }
      this.plantService.imageUrl = this.plant.photo;
      this.lineChartData1[0].data = this.plant.sensor_1Data.map((dat) => dat.value);
      this.lineChartData2[0].data = this.plant.sensor_2Data.map((dat) => dat.value);
      this.lineChartLabels = this.plant.sensor_1Data.map((dat) => dat.date);
      this.plant.ultimo_riegoT = this.plant.ultimo_riego.slice().reverse();
      const date = new Date(this.plant.ultimo_riego[this.plant.ultimo_riego.length - 1]);
      this.plant.ultimo_riego[this.plant.ultimo_riego.length - 1] = (date.getHours() < 10 ? '0' + date.getHours() :  ''
                   + date.getHours()) + ':' + (date.getMinutes() < 10 ? '0' + date.getMinutes() :  '' + date.getMinutes());
    },
    (err) => {
    });
  }

}
