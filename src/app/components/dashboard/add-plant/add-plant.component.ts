import { Component, OnInit } from '@angular/core';
import { PlantService } from '../../../services/plant.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-plant',
  templateUrl: './add-plant.component.html',
  styleUrls: ['./add-plant.component.css']
})
export class AddPlantComponent implements OnInit {

  success: Boolean = false;
  errors: Boolean = false;
  error: String = '';

  selected: String;
  overture: Number;
  caudal: Number;
  maxLastOpenings: Number;

  // Conditions
  weather: Boolean = false;
  timeLapse: String = '24';
  exception1: Boolean = false;
  exception2: Boolean = false;
  exception1Condition: String = 'mayor';
  exception2Condition: String = 'mayor';
  exception1Value: Number = 75;
  exception2Value: Number = 30;
  exception3: Boolean = false;
  exception4: Boolean = false;
  exception3Condition: String = 'menor';
  exception4Condition: String = 'menor';
  exception3Value: Number = 40;
  exception4Value: Number = 30;

  constructor(public plantService: PlantService, private router: Router) {
    this.selected = '1';
    this.overture = 60;
    this.caudal = 4;    // Goteros
    this.maxLastOpenings = 30;
  }

  ngOnInit() {
    this.plantService.imageUrl = 'http://res.cloudinary.com/greenplants/image/upload/w_400,h_400,c_fill/sample.jpg';
  }

  onChange(file) {
    this.plantService.uploader.uploadAll();
  }

  onSubmit(form) {
    console.log(form);
    window.moveBy(0, 0);
    this.success = false;
    this.errors = false;
    if (this.selected !== '1') {
      this.errors = true;
      this.error = 'El Servo numero ' + this.selected + ' no está activado';
      return;
    }
    this.plantService.addPlants(form.name, form.description, form.servo, form.overture, form.caudal, form.maxLastOpenings,
                                 form.weather, this.exception1, this.exception1Condition, this.exception1Value,
                                 this.exception2, this.exception2Condition, this.exception2Value,
                                 this.exception3, this.exception3Condition, this.exception3Value,
                                 this.exception4, this.exception4Condition, this.exception4Value, form.timeLapse)
      .subscribe(data => {
        console.log(data);
        if (data.status === 200) {
          this.success = true;
          setTimeout(() => this.router.navigate(['dashboard']), 1000);
        } else {
          this.errors = true;
          if (data.error.message.indexOf('unique') !== -1) {
            if (data.error.message.indexOf('servo') !== -1) {
              this.error = 'El servo debe ser único para cada plantación';
            } else {
              this.error = 'El nombre debe ser único para cada plantación';
            }
          } else {
            this.error = 'Rellene todas las casillas';
          }
        }
      },
      err => console.log('ERROR: ' + err));
  }

}
