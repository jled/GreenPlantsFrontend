import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PlantService } from '../../services/plant.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {

  }

  logout() {
    const answer = confirm('¿Esta seguro que desea salir?');
    if (answer) {
      localStorage.removeItem('ipAddress');
      localStorage.removeItem('token');
      this.router.navigate(['']);
    }

  }
}
