import { Component, OnInit } from '@angular/core';
import { PlantService } from '../../../services/plant.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-plants',
  templateUrl: './plants.component.html',
  styleUrls: ['./plants.component.css']
})
export class PlantsComponent implements OnInit {

  private success: Boolean = false;
  private errors: Boolean = false;
  private error: String = '';

  dashboardInfo: DashboardInfo = <DashboardInfo>{};
  time = {
    hour: '0',
    minutes: '0'
  };
  public plants: any[];
  constructor(private plantService: PlantService, public router: Router) {
    let date = new Date();
    this.time.hour = date.getHours() < 10 ? '0' + date.getHours() :  '' + date.getHours();
    this.time.minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() :  '' + date.getMinutes();
    this.plants = [];
    setInterval(() => {
      date = new Date();
      this.time.hour = date.getHours() < 10 ? '0' + date.getHours() :  '' + date.getHours();
      this.time.minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() :  '' + date.getMinutes();
    }, 60000);
  }

  ngOnInit() {
    const dataWeather = this.plantService.getWeather()
    .subscribe(data => {
      console.log(data);
      const locationData = data.dataLocation;
      const weatherData = data.dataWeather;
      this.dashboardInfo.street = locationData['1'].long_name;
      this.dashboardInfo.city = locationData['2'].long_name;
      this.dashboardInfo.political = locationData['3'].long_name;
      this.dashboardInfo.temperature = weatherData.temp_c;
      this.dashboardInfo.humidity = weatherData.relative_humidity;
      this.dashboardInfo.icon = weatherData.icon_url;
      if (data.status > 500) {
        console.log('ERROR');
      }
    },
    (err) => {
      console.log('ERROR');
    });

    this.plantService.getPlants()
      .subscribe(data => {
        this.plants = data.plants;
        this.plants.map((plant) => {
          const date = new Date(plant.ultimo_riego[plant.ultimo_riego.length - 1]);
          plant.ultimo_riego[plant.ultimo_riego.length - 1] = (date.getHours() < 10 ? '0' + date.getHours() :  ''
                       + date.getHours()) + ':' + (date.getMinutes() < 10 ? '0' + date.getMinutes() :  '' + date.getMinutes());
        });
        if (data.status > 400) {
        }
      },
      (err) => {
      });

  }

  clickPlant(plant) {
    this.router.navigate(['/dashboard/view', plant._id]);
  }

  editPlant(plant) {
    this.router.navigate(['/dashboard/edit', plant._id]);
  }

  deletePlant(plant) {
    this.success = false;
    this.errors = false;
    const answer = confirm('¿Estas seguro que sea eliminar ' + plant.name + '?');
    if (answer) {
      this.plantService.deletePlant(plant)
      .subscribe(data => {
        if (data.status === 200) {
          this.success = true;
          this.plants = this.plants.filter((planta) => {
            return (plant._id !== planta._id);
          });
        } else {
          this.errors = true;
          this.error = data.error;
        }
      },
      err => {
        console.log('ERROR' + err);
      });
    }
  }
}

export interface DashboardInfo {
  street: String;
  city: String;
  humidity: string;
  temperature: Number;
  icon: string;
  political: string;
}
